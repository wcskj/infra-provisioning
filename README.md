# infra-provisioning

Repozytorium zawierające skrypty do instalowania zależności (tzw. "provisioning") oraz uruchamiające serwisy projektu WCSKJ na maszynach stanowiących infrastrukturę wdrożeniową.

## Wymagania

Do uruchomienia skryptów wdrożeniowych potrzebne jest:

* instalacja [Ansible](https://www.ansible.com/) (wersja 2.8.1)
* plik `ansible/inventory.yml` - zawierający opis (adresy IP) maszyn stanowiących infastrukturę wdrożeniową. Pomimo, że jest integralnym elementem procesu wdrożeniowego, ze względów bezpieczeństwa plik ten **nie jest** częścią tego repozytorium. Wszelkich informacji udziela [administrator grupy repozytoriów projektowych WCSKJ](https://gitlab.com/wcskj).

## ansible

Katalog zawierający skrypty wdrożeniowe [Ansible](https://www.ansible.com/). Główny plik wejściowy to `ansible/playbook.yml`, który stanowi tzw. [Playbook](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html), po którego uruchomieniu automatycznie zainstaluje na maszynie docelowej wszystkie wymagane zależności do uruchomienia składowych projektu WCSKJ.

Pliki playbooków zostały stworzone bazując na następującej wersji Ansible:
```
ansible 2.8.1
  python version = 3.7.3 (default, Mar 27 2019, 09:23:15) [Clang 10.0.1 (clang-1001.0.46.3)]
```

## Vagrantfile

Plik `Vagrantfile` służy do użytku deweloperskiego, stanowi opis maszyny wirtualnej tworzonej za pomocą narzędzia [Vagrant](https://www.vagrantup.com/), na której można testowo uruchamiać skrypty Ansible (patrz podsekcja *ansible*).

## Kopie zapasowe

Playbooki w tym repozytorium umożliwiają tworzenie i przywracanie kopii zapasowych obu serwisów WCSKJ - publicznego oraz Intranetu.

### Tworzenie

```bash
ansible-playbook -i [ŚCIEŻKA DO PLIKU INVENTORY] ansible/[SERWIS]/backup.yml
```

gdzie odpowiednio: